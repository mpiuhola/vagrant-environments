# opentrons-api-env

Simple VM for using the opentrons API (https://docs.opentrons.com/).

Based on geerlingguy's Ubuntu 18.04 LTS Vagrant box.

## Prerequisites

- Install VirtualBox (>= 5.2.10)
- Install Vagrant (>= 2.0.0)
- Install vagrant-vbguest plugin: `vagrant plugin install vagrant-vbguest`
- Get an opentrons robot

## Usage

1. Clone this repository
2. Open a terminal into this (`opentrons-api-env/`) directory
3. Run:

    vagrant up
    vagrant ssh

4. Do Python stuff
