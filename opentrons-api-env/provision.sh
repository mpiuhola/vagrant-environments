#!/bin/bash -e

# Make Python 3.6 the default
echo 'alias python=python3.6' >> /home/vagrant/.bashrc

echo 'Installing pip...'
sudo apt-get update
sudo apt-get install --yes python-pip python3-setuptools

echo 'Installing pipenv for user "vagrant"...'
su --login vagrant <<-EOF
	pip install --user pipenv
	echo 'export PATH="${PATH}:/home/vagrant/.local/bin"' >> ~/.bashrc
EOF

echo 'Setting up opentrons environment...'
su --login vagrant <<-EOF
	set -o errexit
	git clone https://github.com/Opentrons/opentrons.git
	cd opentrons/api
	make install
EOF

cat <<-EOF >> /home/vagrant/.bashrc
	cd ~/opentrons/api
	pipenv shell
EOF

cat <<-EOF

	To use the opentrons environment,
	run: vagrant ssh

	You can now run your Python scripts against OT-2.

EOF
